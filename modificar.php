<?php
	include_once('conexion/cnx.php');
  $idpersona = $_REQUEST['id'];
	$QBP = $cnx->query("SELECT * FROM personas where idpersona='$idpersona' ");
	while ( $fila = mysqli_fetch_array($QBP) )
	{
		$nombre    = $fila['nombre'];
		$apellido  = $fila['apellido'];
		$cedula    = $fila['cedula'];
		$telefono  = $fila['telefono'];
		$direccion = $fila['direccion'];
		$correo    = $fila['correo'];
	}
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Actividad 1 - CRUD</title>
    <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php
      include_once('./nav.php')
    ?>
    
    <div class="centerContent">
      <div class="form-group col-md-6 titleMargin">
        <p class="display-4 text-center">Modificar Persona</p>
      </div>
      <form class="container centerContent" action="crud/dataMod.php?id=<?php echo $idpersona ?>" class="form-horizontal" method="POST">
        <div class="form-group col-md-6">
          <input name="nombre" type="text" placeholder="Nombre" class="form-control" value="<?php echo $nombre ?>">
        </div>
        <div class="form-group col-md-6">
          <input name="apellido" type="text" placeholder="Apellido" class="form-control" value="<?php echo $apellido ?>">
        </div>
        <div class="form-group col-md-6">
          <input name="cedula" type="text" placeholder="Cedula" class="form-control" value="<?php echo $cedula ?>">									
        </div>
        <div class="form-group col-md-6">
          <input name="telefono" type="text" placeholder="Telefono" class="form-control" value="<?php echo $telefono ?>">
        </div>
        <div class="form-group col-md-6">
          <input name="direccion" type="text" placeholder="Direccion" class="form-control" value="<?php echo $direccion ?>">
        </div>
        <div class="form-group col-md-6">
          <input name="correo" type="email" placeholder="Correo" class="form-control" value="<?php echo $correo ?>">
        </div>
        <div class="form-row justify-content-center">
          <input name="enviar" type="submit" value="Modificar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </body>
</html>