<?php
	include_once('conexion/cnx.php');
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Actividad 1 - CRUD</title>
    <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php
      include_once('./nav.php')
    ?>

    <?php
      $QBPersonas = "SELECT * FROM personas";
      $QB = mysqli_query($cnx, $QBPersonas) or die(mysqli_error($cnx));
      if (mysqli_num_rows($QB) > 0 ) 
      {
    ?>
    <h1 class="text-center titleMargin">CONSULTA DE PERSONAS</h1>
    <div class="centerContent">
      <table class="table tableCrud">
        <tr>
          <td><strong>N°</strong></td>
          <td><strong>Nombre</strong></td>
          <td><strong>Apellido</strong></td>
          <td><strong>Cédula</strong></td>
          <td><strong>Teléfono</strong></td>
          <td><strong>Dirección</strong></td>
          <td><strong>Correo</strong></td>
          <td><strong>Acciones</strong></td>
          <td></td>
        </tr>
        <?php
          $nro = 0;
          while ( $fila = mysqli_fetch_array($QB) )
          {
            $nro++;
            $idpersona = $fila['idpersona'];
            $nombre    = $fila['nombre'];
            $apellido  = $fila['apellido'];
            $cedula    = $fila['cedula'];
            $telefono  = $fila['telefono'];
            $direccion = $fila['direccion'];
            $correo    = $fila['correo'];
        ?>
        <tr>
          <td><?php echo $nro ?></td>
          <td><?php echo $nombre ?></td>
          <td><?php echo $apellido ?></td>
          <td><?php echo $cedula ?></td>
          <td><?php echo $telefono ?></td>
          <td><?php echo $direccion ?></td>
          <td><?php echo $correo ?></td>
          <td><a href="modificar.php?id=<?php echo $idpersona ?>" class="btn bg-success btnCrud">Modificar</a></td>
          <td><button class="btn bg-danger btnCrud" data-toggle="modal" data-target="#modalDelete" role="button">Eliminar</button></td>
        </tr>

        <div class="modal fade" id="modalDelete">
          <div class="modal-dialog">
            <div class="modal-content">
              <!--Modal Header-->
              <div class="modal-header">
                <h4 class="modal-title">Alerta!</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
              </div>
              <!--Modal body-->
              <div class="modal-body">
                <h5>Desea eliminar a la persona seleccionada?</h5>
              </div>
              <!--Modal footer-->
              <div class="modal-footer">
                <a href="crud/eliminar.php?id=<?php echo $idpersona ?>"><button type="button" class="btn btn-outline-danger">Eliminar</button></a>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
      </table>
    </div>
    
    <?php }else{ ?>
      <h2 class="text-center titleMargin">NO HAY NINGUNA PERSONA REGISTRADA</h2>
    <?php } ?>
  </body>
</html>