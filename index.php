<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Actividad 1 - CRUD</title>
    <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/estilos.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  </head>
  <body>
    <?php
      include_once('./nav.php')
    ?>

    <div class="centerContent">
      <div class="form-group col-md-6 titleMargin">
        <p class="display-4 text-center">Registro de Personas</p>
      </div>
      <form class="container centerContent" action="crud/dataAddPerson.php" class="form-horizontal" method="POST">
        <div class="form-group col-md-6">
          <input name="nombre" type="text" placeholder="Nombre" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <input name="apellido" type="text" placeholder="Apellido" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <input name="cedula" type="text" placeholder="Cedula" class="form-control">									
        </div>
        <div class="form-group col-md-6">
          <input name="telefono" type="text" placeholder="Telefono" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <input name="direccion" type="text" placeholder="Direccion" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <input name="correo" type="email" placeholder="Correo" class="form-control">
        </div>
        <div class="form-row justify-content-center">
          <input name="enviar" type="submit" value="Registrar" class="btn btn-primary">
        </div>
      </form>
    </div>
  </body>
</html>