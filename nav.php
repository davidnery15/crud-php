<header>
  <nav class="nav navbar navbar-expand-lg navbar-dark bg-dark navbar-inverse">
    <div class="container">
      <div class="navbar-header">
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <p class="nav-link navbar-brand">URBE CRUD</p>
      </div>
      <div class="collapse navbar-collapse justify-content-end" id="navbarResponsive">
        <a class="nav-link mp text-white" href="index.php">Registrar Personas</a>
        <a class="nav-link mp text-white" href="personas.php">Consultar Personas</a>
      </div>
    </div>
  </nav>
</header>